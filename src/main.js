var jan = (function() {

	var self = {

		init: function() {

			// Loader info
			console.log( 'jQuery has been loaded successfully' );

			/**
			 * Init Input listener
			 */
			this.inputListener();
		},

		inputListener: function () {

			var firstname = '';
			var lastname = '';

			$( 'input' ).on( 'keyup', function() {

				var $elem = $( this );
				var $resp = $( 'p.response' );

				if ( $elem.attr( 'id' ) == 'inputFirstname' )
					firstname = $elem.val();
				else if ( $elem.attr( 'id' ) == 'inputLastname' )
					lastname = $elem.val();

				if ( firstname != '' )
					$resp.html( 'Guten Tag ' + firstname + ' ' + lastname );
				else
					$resp.html( '' );
			} );
		}
	};

	// API
	return {
		init: self.init()
	}

})();

$( document ).ready( function() {
	jan.init;
} );